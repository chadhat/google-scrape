import argparse
from urllib.parse import urlparse

from googlesearch import search


def google_search(query, num_results=10, exclude_domains=None):
    if exclude_domains is None:
        exclude_domains = []

    try:
        # Perform the search
        results = search(query, num=num_results,stop=num_results)

        # Filter out unwanted domains
        filtered_results = [
            result
            for result in results
            if not any(domain in result for domain in exclude_domains)
        ]

        # Extract and print the root URLs with scheme
        root_urls = [
            f"{urlparse(result).scheme}://{urlparse(result).netloc}"
            for result in filtered_results[:num_results]
        ]
        root_urls = list(dict.fromkeys(root_urls))
        for idx, root_url in enumerate(root_urls, start=0):
            print(f"{idx+1}. {root_url}")
    except Exception as e:
        print(f"An error occurred: {e}")


def process_queries_from_file(filename, num_results=10, exclude_domains=None):
    try:
        with open(filename, "r") as file:
            queries = file.readlines()

        for query in queries:
            query = query.strip()
            if query:  # Process only non-empty queries
                print(f"\nResults for query: {query}")
                google_search(query, num_results, exclude_domains)
    except Exception as e:
        print(f"An error occurred while reading the file: {e}")


if __name__ == "__main__":
    # Example usage
    # filename = "queries.txt"
    # num_results = 5
    # exclude_domains = ["ethz.ch", "uzh.ch", "scholar.google", "linkedin.com"]
    # process_queries_from_file(filename, num_results, exclude_domains)

    parser = argparse.ArgumentParser(
        description="Google search script with domain exclusion"
    )
    parser.add_argument(
        "--query_file",
        type=str,
        help="The name of the file containing queries",
        default="queries.txt",
    )
    parser.add_argument(
        "--num_results",
        type=int,
        help="Number of search results to retrieve",
        default=6,
    )
    parser.add_argument(
        "--exclude_domains",
        type=str,
        nargs="+",
        help="Domains to exclude from search results",
        default=["person.details", "person-details", "uzh.ch", "scholar.google", 
                 "linkedin.com", "https://bsse.ethz.ch", "https://ethz.ch", "https://inf.ethz.ch",
                 "https://ee.ethz.ch", "https://bsse.ethz.ch", "https://mavt.ethz.ch", "https://www.mat.ethz.ch",
                 "https://biol.ethz.ch", "https://chab.ethz.ch", "https://phys.ethz.ch", "https://www.erdw.ethz.ch",
                 "https://usys.ethz.ch", "https://hest.ethz.ch", "https://mtec.ethz.ch", "https://gess.ethz.ch"],
    )

    args = parser.parse_args()

    process_queries_from_file(args.query_file, args.num_results, args.exclude_domains)
