# google-scrape

# Google Search Script with Domain Exclusion

This script allows you to perform Google searches from the command line, filtering out results from specified domains. It reads queries from a file, performs the searches, and prints the root URLs of the filtered results.

## Features

- Perform Google searches from the command line.
- Read queries from a supplied file.
- Filter out results from specified domains.
- Print the root URLs with the scheme.

## Installation

1. **Clone the repository:**
    ```
    git clone https://gitlab.ethz.ch/chadhat/google-scrape.git
    cd google-scrape
    ```

2. **Install the required library:**
    ```
    python3 -m venv env
    source env/bin/activate
    pip install googlesearch-python 
    ```

## Usage

### Command-line Arguments

- `--query_file` (optional, default: queries.txt): The name of the file containing queries.
- `--num_results` (optional, default: 5): Number of search results to retrieve.
- `--exclude_domains` (optional, default: ["ethz.ch", "uzh.ch", "scholar.google", "linkedin.com"]): Domains to exclude from search results.

### Running the Script

1. **Prepare a file with your queries:**

    Create a file named `queries.txt` with one query per line. For example:
    ```
    Professor A
    Professor B
    ```

2. **Run the script:**

    Use the following command to run the script with the default settings:
    ```
    python main.py
    ```

    To specify the query file, number of results and exclude specific domains, use:
    ```sh
    python main.py --query_file queries.txt --num_results 5 --exclude_domains ethz.ch uzh.ch scholar.google linkedin.com
    ```